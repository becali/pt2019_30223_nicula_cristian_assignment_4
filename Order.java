package PT2019.tema4.tema4;
import java.time.LocalDate;
public class Order {
	int OrderID, Table;
	LocalDate date;
	Order(int id, int tab)
	{
		this.OrderID = id;
		this.Table = tab;
		this.date = LocalDate.now();
	}
	
	public int hashCode()
	{
		return (this.OrderID % 5 + this.Table) % 5;
	}
}
