package PT2019.tema4.tema4;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import javax.swing.*;

public class Observer extends JPanel {
	
	private static final long serialVersionUID = 1L;

	Observer(List<MenuItem> comanda)
	{
		
		JFrame cook = new JFrame();
		JPanel p = new JPanel();
		p.setLayout(null);
		JLabel fa = new JLabel("NEW ORDER!");
		
		cook.setSize(700, 500);
		cook.setContentPane(p);
		JTextArea ta= new JTextArea(5, 30);
		JScrollPane scroll = new JScrollPane(ta);
		
		fa.setBounds(270, 50, 300, 50);
		scroll.setBounds(25, 100, 600, 340);
		fa.setFont(new Font("Arial", Font.PLAIN, 30));
		ta.setFont(new Font("Arial", Font.PLAIN, 30));
		ta.setBackground(Color.getHSBColor(55, 75, 89));
    	p.setBackground(Color.getHSBColor(55, 75, 89));
    	
		String com = new String();
		for(MenuItem x : comanda)
		{
			com = com.concat(x.name+" \n");
		}
		ta.setText(com);
		
		
		p.add(fa);
		p.add(scroll);
		cook.setVisible(true);
	}
}
