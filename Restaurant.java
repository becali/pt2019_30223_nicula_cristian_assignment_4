package PT2019.tema4.tema4;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.*;
public class Restaurant implements RestaurantProcessing {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<MenuItem> meniu=new ArrayList();
	HashMap <Order, List<MenuItem>> comenzi = new HashMap<Order, List<MenuItem>>();
	Restaurant()
	{
		MenuItem unu = new MenuItem("Ciorba ardeleneasca", 2.6);
		MenuItem doi = new MenuItem("Ciorba taraneasca", 2.2);
		MenuItem trei = new MenuItem("Pilaf cu pui", 5.5);
		MenuItem patru = new MenuItem("Piure cu piept de pui", 8.2);
		MenuItem cinci = new MenuItem("Baclava", 4);
		meniu.add(unu);
		meniu.add(doi);
		meniu.add(trei);
		meniu.add(patru);
		meniu.add(cinci);
	}

	public void addMenuItem(MenuItem x) {
		// TODO Auto-generated method stub
		meniu.add(x);
	}

	public int removeMenuItem(MenuItem x) {
		// TODO Auto-generated method stub
		if(meniu.remove(x) == true)
			return 1;
		return 0;
	}

	public void editMenuItem(MenuItem x, String name, double price) {
		// TODO Auto-generated method stub
		meniu.remove(x);
		MenuItem y = new MenuItem(name,price);
		meniu.add(y);
	}
	
	int id;
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public void newOrder(Vector<Integer> v, int table)
	{
		Order comanda = new Order(id,table);
		List<MenuItem> iteme=new ArrayList();
		int i=0;
		while(i<v.size())
		{
			iteme.add(meniu.get(v.get(i) - 1));
			i++;
		}
		Observer chef = new Observer(iteme);
		comenzi.put(comanda, iteme);
		id++;
	}
	
	public String[][] viewOrders()
	{
		String data[][] = new String[50][25];
		int i = 0;
		for(Order key : comenzi.keySet())
		{
			int j=3;
			int pret = 0;
			data[i][0] = String.valueOf(key.OrderID);
			data[i][1] = String.valueOf(key.Table);
			int n = comenzi.get(key).size();
			while(j < n	+ 3)
			{
				data[i][j] = comenzi.get(key).get(j - 3).name;
				pret += comenzi.get(key).get(j - 3).price;
				j++;
			}
			data[i][2] = String.valueOf(pret);
			i++;
		}
		return data;
	}
	
	public boolean billz(int table) throws UnsupportedEncodingException, FileNotFoundException, IOException
	{
		String comanda = new String();
		int  pret = 0;
		for(Order key : comenzi.keySet())
		{
			if(key.Table == table)
			{
				int i = 0;
				while(i < comenzi.get(key).size())
				{
					comanda = comanda.concat(comenzi.get(key).get(i).name+": "+comenzi.get(key).get(i).price+" ");
					pret += comenzi.get(key).get(i).price;
					i++;
				}
			}
				
		}
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Bill.txt"), "utf-8"))) 
		{
			writer.write("Total: " + pret +  " lei "+"Produse: " +comanda);	
		}
		
		return false;
	}
}
