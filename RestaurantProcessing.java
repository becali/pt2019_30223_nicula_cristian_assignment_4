package PT2019.tema4.tema4;

import java.util.Vector;

public interface RestaurantProcessing {
	public void addMenuItem(MenuItem x);
	public int removeMenuItem(MenuItem x);
	public void editMenuItem(MenuItem x,String name, double price);
	public void newOrder(Vector<Integer> v, int table);
	public String[][] viewOrders();
}
