package PT2019.tema4.tema4;



import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Vector;



public class Admin extends JPanel {

	private static final long serialVersionUID = 1L;

	JFrame pagina_login = new JFrame(), pagina_admin = new JFrame(), p_a_add = new JFrame(), p_a_edit = new JFrame(), p_a_remove = new JFrame();
	JFrame view = new JFrame();
	JPanel p_view = new JPanel();
	JPanel p_login = new JPanel(), p_admin = new JPanel(),p_add = new JPanel(), p_edit = new JPanel(), p_remove = new JPanel();
	JTextField tf_nume = new JTextField(), tf_parola = new JTextField();
	JLabel e_nume = new JLabel("Id:"), e_parola = new JLabel("Password:");
	JButton ok = new JButton("Ok");
	Restaurant Dinel = new Restaurant();
	JTextField tf_add_nume = new JTextField(), tf_add_pret = new JTextField();
	JTextField tf_remove_nume = new JTextField(), tf_remove_pret = new JTextField();
	JTextField tf_edit_nume = new JTextField(), tf_edit_pret = new JTextField(), tf_edit_numenou = new JTextField();
	
	
	Vector<Integer> v = new Vector<Integer>();
	JFrame pagina_waiter = new JFrame(), f_new_order = new JFrame(), f_view_orders = new JFrame(), f_bill = new JFrame();
	JPanel p_waiter = new JPanel(), p_view_orders = new JPanel(), p_new_order = new JPanel(), p_bill = new JPanel();
	JTextField comand = new JTextField();
	JTextField tf = new JTextField();
	JButton Ok_Bill = new JButton("OK");
	JFrame yea = new JFrame();
	JPanel p = new JPanel();
	
	int masa = 0;
	@SuppressWarnings("null")
	public Admin() {
		
		//PAGINA LOGIN ////////////////////////////////////////////////////////////////////
		
		pagina_login.setSize(800,  535);
		p_login.setLayout(null);
		
		ok.setBounds(225, 265, 75, 50);
		tf_nume.setBounds(50, 100, 250, 50);
		tf_parola.setBounds(50, 200, 250, 50);
		e_nume.setBounds(50, 75, 100, 25);
		e_parola.setBounds(50, 175, 300, 25);
		
		e_nume.setFont(new Font("Arial", Font.PLAIN, 28));
		e_parola.setFont(new Font("Arial", Font.PLAIN, 28));
		tf_nume.setFont(new Font("Arial", Font.PLAIN, 28));
		tf_parola.setFont(new Font("Arial", Font.PLAIN, 28));
		
		p_login.add(e_nume);
		p_login.add(e_parola);
		p_login.add(ok);
		p_login.add(tf_nume);
		p_login.add(tf_parola);
		p_login.setBackground(Color.getHSBColor(55, 75, 89));
		pagina_login.setContentPane(p_login);
		pagina_login.setVisible(true);
		
		class ButtonListenerok implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	if(tf_nume.getText().equals("admin") && tf_parola.getText().equals("1234"))
	        	{
	        		pagina_login.setVisible(false);
	        		pagina_admin.setVisible(true);
	        	}
	        	else if(tf_nume.getText().equals("waiter") && tf_parola.getText().equals("1234"))
	        	{
	        		pagina_login.setVisible(false);
	        		pagina_waiter.setVisible(true);
	        	}
	        }
	    } 
		
		ButtonListenerok ok1 = new ButtonListenerok();
		ok.addActionListener(ok1);
		
		// PAGINA ADMIN ///////////////////////////////////////////////////////////////////
		
		pagina_admin.setSize(800, 535);
		pagina_admin.setContentPane(p_admin);
		p_admin.setLayout(null);
		p_admin.setBackground(Color.getHSBColor(55, 75, 89));
		
		JLabel admin = new JLabel("ADMIN");
		JButton add = new JButton("Add Menu Item");
		JButton remove = new JButton("Remove Menu Item");
		JButton edit = new JButton("Edit Menu Item");
		JButton tabel = new JButton("View Menu Items");
		JButton ok_add = new JButton("Add to Menu");
		JButton back = new JButton("Waiter");
		
				
		back.setBounds(50, 400, 200, 75);
		ok_add.setBounds(25, 325, 200, 75);
		add.setBounds(50,100,300,100);
		remove.setBounds(360, 100, 300, 100);
		edit.setBounds(50, 250, 300, 100);
		tabel.setBounds(360, 250, 300, 100);
		admin.setBounds(300, 50, 300, 40);
		admin.setFont(new Font("Arial", Font.PLAIN, 38));

		p_admin.add(add);
		p_admin.add(remove);
		p_admin.add(edit);
		p_admin.add(tabel);
		p_admin.add(admin);
		p_admin.add(back);
		
		// ADD ///////////////////////////////////////////////////////////////////////////////////////
		
		p_a_add.setSize(500, 500);
		p_a_add.setContentPane(p_add);
		p_add.setLayout(null);
		p_add.setBackground(Color.getHSBColor(55, 75, 89));
		
		JLabel e1 = new JLabel("Name of the item:");
		JLabel e2 = new JLabel("Price of the item:");
		
		e1.setBounds(25, 25, 350, 45);
		e2.setBounds(25, 175, 300, 45);
		tf_add_nume.setBounds(25,75,400,75);
		e1.setFont(new Font("Arial", Font.PLAIN, 30));
		e2.setFont(new Font("Arial", Font.PLAIN, 30));
		tf_add_nume.setFont(new Font("Arial", Font.PLAIN, 20));
		tf_add_pret.setFont(new Font("Arial", Font.PLAIN, 20));
		tf_add_pret.setBounds(25,215,200, 75);
		p_add.add(ok_add);
		p_add.add(tf_add_nume);
		p_add.add(tf_add_pret);
		p_add.add(e1);
		p_add.add(e2);

		// REMOVE ///////////////////////////////////////////////////////////////////////////////////////
		
		p_a_remove.setSize(500, 500);
		p_a_remove.setContentPane(p_remove);
		p_remove.setLayout(null);
		p_remove.setBackground(Color.getHSBColor(55, 75, 89));
		
		JButton ok_remove = new JButton("Remove Item");
		JLabel e3 = new JLabel("Name of the item:");
		
		ok_remove.setBounds(25, 150, 200, 50);
		e3.setBounds(25, 25, 350, 45);
		e3.setFont(new Font("Arial", Font.PLAIN, 30));
		
		tf_remove_nume.setBounds(25,75,400,75);
		tf_remove_nume.setFont(new Font("Arial", Font.PLAIN, 20));
		
		
		
		p_remove.add(ok_remove);
		p_remove.add(tf_remove_nume);
		p_remove.add(e3);
		
		// EDIT ///////////////////////////////////////////////////////////////////////////////////////
		
		p_a_edit.setSize(500, 550);
		p_a_edit.setContentPane(p_edit);
		p_edit.setLayout(null);
		p_edit.setBackground(Color.getHSBColor(55, 75, 89));
		
		JButton ok_edit = new JButton("Edit Item");
		JLabel e4 = new JLabel("Name of the item:");
		JLabel e5 = new JLabel("New name:");
		JLabel e6 = new JLabel("New Price:");
		e4.setFont(new Font("Arial", Font.PLAIN, 30));
		e5.setFont(new Font("Arial", Font.PLAIN, 30));
		e6.setFont(new Font("Arial", Font.PLAIN, 30));
		
		e4.setBounds(25, 25, 350, 45);
		e5.setBounds(25, 150, 350, 45);
		e6.setBounds(25, 275, 350, 45);
		ok_edit.setBounds(25, 425, 200, 50);
		
		tf_edit_nume.setBounds(25,75,400,75);
		tf_edit_nume.setFont(new Font("Arial", Font.PLAIN, 20));
		tf_edit_numenou.setBounds(25,200,400,75);
		tf_edit_numenou.setFont(new Font("Arial", Font.PLAIN, 20));
		tf_edit_pret.setBounds(25,325,400,75);
		tf_edit_pret.setFont(new Font("Arial", Font.PLAIN, 20));
		
		p_edit.add(tf_edit_nume);
		p_edit.add(tf_edit_numenou);
		p_edit.add(tf_edit_pret);
		p_edit.add(ok_edit);
		p_edit.add(e4);
		p_edit.add(e5);
		p_edit.add(e6);
		
		// JTABLE ADMIN /////////////////////////////////////////////////////////////////////////////
	    
		class Recalculam
		{
			public void recalc()
			{
				view = new JFrame();
				//System.out.println("Merg");
				String data[][] = new String[50][5];
	    		int i=0;
	    		int n = Dinel.meniu.size();
	    		for(i=0;i<n;i++)
	    		{
	    			i++;
	    			data[i-1][0] = String.valueOf(i);
	    			i--;
	    			data[i][1] = Dinel.meniu.get(i).name;
	    			data[i][2] = String.valueOf(Dinel.meniu.get(i).price);
	    		}
	    	    String column[]={"ID", "NAME","PRICE"};         
	    	    JTable jt=new JTable(data,column);  
	    	    jt.setFont(new Font("Arial", Font.PLAIN, 20));
	    	    JScrollPane sp=new JScrollPane(jt);   
	    	    sp.setBackground(Color.getHSBColor(55, 75, 89));
	    	    jt.setBackground(Color.getHSBColor(55, 75, 89));
	    	    view.add(sp);          
	    	    view.setSize(700,500);
			}
		}
	  
		// BUTOANE ADMIN /////////////////////////////////////////////////////////////////
		
		class Bec implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	pagina_admin.setVisible(false);
	        	pagina_waiter.setVisible(true);
	        }
	    }
		
		Bec becali = new Bec();
		back.addActionListener(becali);
		
		class ButtonListeneradd implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	p_a_add.setVisible(true);
	        }
	    }
		class ButtonListenerremove implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	p_a_remove.setVisible(true);
	        }
	    }
		class ButtonListeneredit implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	p_a_edit.setVisible(true);
	        }
	    }
		class ButonTabel implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Recalculam da = new Recalculam();
	        	da.recalc();
	        	view.setVisible(true);
	        	
	        }
	    }
		
		class ButtonListeneradd_menu implements ActionListener  // adaugarea unui item in meniu
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	MenuItem x = new MenuItem(tf_add_nume.getText(),Double.parseDouble(tf_add_pret.getText()));
	        	tf_add_nume.setText("");
	        	tf_add_pret.setText("");
	        	Dinel.addMenuItem(x);
	        	p_a_add.setVisible(false);
	        }
	    }
		
		class ButtonListenerremove_menu implements ActionListener  // scoaterea unui item din meniu
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	int i=0;
	        	while(i< Dinel.meniu.size())
	        	{
	        		MenuItem x = Dinel.meniu.get(i);
	        		if(x.name.equals(tf_remove_nume.getText()))
	        			Dinel.removeMenuItem(x);
	        		i++;
	        	}   	
	        	p_a_remove.setVisible(false);        	
	        }
	    }
		
		class ButonEdit implements ActionListener  // scoaterea unui item din meniu
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	int i=0, ok=0;
	        	while(i< Dinel.meniu.size())
	        	{
	        		MenuItem x = Dinel.meniu.get(i);
	        		if(x.name.equals(tf_edit_nume.getText()))
	        			if(Dinel.removeMenuItem(x) == 1) 
	        				ok = 1;
	        		i++;
	        	}
	        	MenuItem y = new MenuItem(tf_edit_numenou.getText(), Double.parseDouble(tf_edit_pret.getText()));
	        	if(ok == 1) 
	        		Dinel.addMenuItem(y);
	        	p_a_edit.setVisible(false);
	        	/*for(MenuItem yx: Dinel.meniu)
	        		System.out.println(yx.name);*/
	        }
	    }
		
		ButtonListeneradd a = new ButtonListeneradd();
		ButtonListeneradd_menu b = new ButtonListeneradd_menu();
		ButtonListenerremove c = new ButtonListenerremove();
		ButtonListenerremove_menu d = new  ButtonListenerremove_menu();
		ButtonListeneredit e = new ButtonListeneredit();
		ButonEdit f = new ButonEdit();
		ButonTabel g = new ButonTabel();
		
		add.addActionListener(a);
		ok_add.addActionListener(b);
		remove.addActionListener(c);
		ok_remove.addActionListener(d);
		edit.addActionListener(e);
		ok_edit.addActionListener(f);
		tabel.addActionListener(g);		
		
		// WAITER //////////////////////////////////////////////////////////////////////////////////////////
		
		pagina_waiter.setSize(770,  450);
		pagina_waiter.setContentPane(p_waiter);
		p_waiter.setLayout(null);
		p_waiter.setBackground(Color.getHSBColor(55, 75, 89));
		
		JLabel ee1 = new JLabel("WAITER");
		JButton new_order = new JButton("New Order"), view_orders = new JButton("View Orders"), bill = new JButton("Compute Bill");
		JButton login = new JButton("Back to login");

		ee1.setFont(new Font("Arial", Font.PLAIN, 38));
		ee1.setBounds(285, 50, 300, 40);
		new_order.setBounds(50, 100, 300, 100);
		view_orders.setBounds(360, 100, 300, 100);
		bill.setBounds(205, 210, 300, 100);
		login.setBounds(255, 330, 200, 55);
		
		p_waiter.add(new_order);
		p_waiter.add(view_orders);
		p_waiter.add(bill);
		p_waiter.add(ee1);
		p_waiter.add(login);
		
		// NEW ORDER ////////////////////////////////////////////////////////////////////////////////
		
		f_new_order.setSize(500, 500);
		p_new_order.setLayout(new GridLayout(0,2));
		f_new_order.setContentPane(p_new_order);
		p_new_order.setBackground(Color.getHSBColor(55, 75, 89));
		
		JButton un = new JButton("1"), doi = new JButton("2"), tr = new JButton("3"), pa = new JButton("4"), ci = new JButton("5"), 
		sas = new JButton("6"), sap = new JButton("7"), op = new JButton("8"), no = new JButton("9"), ze = new JButton("10"), uns = new JButton("11"),
		dois = new JButton("12"), trei = new JButton("13"), pai = new JButton("14"), gata = new JButton("Done");
		p_new_order.add(un);
		p_new_order.add(doi);
		p_new_order.add(tr);
		p_new_order.add(pa);
		p_new_order.add(ci);
		p_new_order.add(sas);
		p_new_order.add(sap);
		p_new_order.add(op);
		p_new_order.add(no);
		p_new_order.add(ze);
		p_new_order.add(uns);
		p_new_order.add(dois);
		p_new_order.add(trei);
		p_new_order.add(pai);
		p_new_order.add(comand);
		p_new_order.add(gata);
		
		class bec_log implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	pagina_waiter.setVisible(false);
	        	pagina_login.setVisible(true);
	        }
	    } 
		bec_log beca = new bec_log();
		login.addActionListener(beca);
		
		
		class NewOr implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	Recalculam x = new Recalculam();
	        	x.recalc();
	        	view.setVisible(true);
	        	f_new_order.setVisible(true);
	        }
	    } 
		
		NewOr aa = new NewOr();
		new_order.addActionListener(aa);
		
		class B1 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "1 ");
	        }
	    }
		
		class B2 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "2 ");
	        }
	    }

		class B3 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "3 ");
	        }
	    }
		
		class B4 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "4 ");
	        }
	    }
		
		class B5 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "5 ");
	        }
	    }
		
		class B6 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "6 ");
	        }
	    }
		
		class B7 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "7 ");
	        }
	    }
		
		class B8 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "8 ");
	        }
	    }
		
		class B9 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "9 ");
	        }
	    }
		
		class B10 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "10 ");
	        }
	    }
		
		class B11 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "11 ");
	        }
	    }
		
		class B12 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "12 ");
	        }
	    }
		
		class B13 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "13 ");
	        }
	    }
		
		class B14 implements ActionListener
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	comand.setText(comand.getText() + "14 ");
	        }
	    }
		
		B1 ab = new B1();  B9 aj = new B9();
		B2 ac = new B2();  B10 ak = new B10();
		B3 ad = new B3();  B11 al = new B11();
		B4 ae = new B4();  B12 am = new B12();
		B5 af = new B5();  B13 an = new B13();
		B6 ag = new B6();  B14 ao = new B14();
		B7 ah = new B7();
		B8 ai = new B8();
		
		un.addActionListener(ab);
		doi.addActionListener(ac);
		tr.addActionListener(ad);
		pa.addActionListener(ae);
		ci.addActionListener(af);
		sas.addActionListener(ag);
		sap.addActionListener(ah);
		op.addActionListener(ai);
		no.addActionListener(aj);
		ze.addActionListener(ak);
		uns.addActionListener(al);
		dois.addActionListener(am);
		trei.addActionListener(an);
		pai.addActionListener(ao);
		
		comand.setEditable(false);
		
		class GataOrder implements ActionListener   /// se introduce comanda, se apasa Done si apoi se introduce nr mesei si se apasa iar Done
	    {
	        public void actionPerformed(ActionEvent e)
	        {
	        	if(masa==0) {
	        	String text = new String();
	        	v = new Vector<Integer>();
	        	text = comand.getText();
	        	int i = 0, n = text.length();
	        	while(i<n)
	        	{
	        		int nr = 0;
	        		while(Character.isDigit(text.charAt(i)))
	        		{
	        			if(i<n)
	        				nr = nr*10 + text.charAt(i) - '0';
	        			i++;
	        		}
	        		if(Character.isDigit(text.charAt(i)) == false)  /// adaugam in vector itemele comenzii
	        			if(nr != 0 && masa == 0)
	        				v.add(nr);
	        		i++;
	        	}
	        	if(masa == 0)
	        	{
	        		comand.setText("");
	        		masa = 1;
	        	}}
	        	else
	        	{
	        		int numar = 0, index = 0;
	        		String tf = comand.getText();
	        		while(index < tf.length())
	        		{
	        			while(Character.isDigit(tf.charAt(index)))
	        			{
	        				numar = numar *10 +  tf.charAt(index) - '0';       				
	        				index++;
	        			}
	        			index++;
	        		}
	        		masa = 0;
	        		Dinel.newOrder(v, numar);
	        		comand.setText("");
	        		f_new_order.setVisible(false);
	        	}
	        }
	    }
		GataOrder a1 = new GataOrder();
		gata.addActionListener(a1);
		
		// VIEW ORDERS /////////////////////////////////////////////////////////////////////////////////////////////
		
		class ViewOrderss implements ActionListener
		{
	        public void actionPerformed(ActionEvent e)
	        {
	        	f_view_orders = new JFrame();
	        	f_view_orders.setSize(1000, 500);
	        	String data2[][] = Dinel.viewOrders();
	        	String column[]= {"ID", "TABLE", "PRICE","ITEM", "ITEM", "ITEM", "ITEM","ITEM", "ITEM"};         
	    	    JTable jt2=new JTable(data2, column);  
	    	    jt2.setFont(new Font("Arial", Font.PLAIN, 20));
	    	    JScrollPane sp=new JScrollPane(jt2);   
	    	    sp.setBackground(Color.getHSBColor(55, 75, 89));
	    	    jt2.setBackground(Color.getHSBColor(55, 75, 89));
	    	    f_view_orders.add(sp);          
	    	    f_view_orders.setVisible(true);
	        }
	    }
		
		ViewOrderss a2 = new ViewOrderss();
		view_orders.addActionListener(a2);
		
		// COMPUTE BILLS ////////////////////////////////////////////////////////////////////////////////////////////////////
		
		JLabel masacare = new JLabel("Bon pentru masa cu numarul:");
		yea.setSize(430,300);
		
		masacare.setBounds(50, 50, 370, 45);
		masacare.setFont(new Font("Arial", Font.PLAIN, 25));
    	tf.setBounds(50, 100, 300, 75);
    	tf.setFont(new Font("Arial", Font.PLAIN, 40));
    	Ok_Bill.setBounds(150, 200, 100, 25);
    	p.add(tf);
    	p.add(Ok_Bill);
    	p.add(masacare);
    	p.setBackground(Color.getHSBColor(55, 75, 89));
    	yea.setContentPane(p);
    	p.setLayout(null);

    	
		class Billuri implements ActionListener
		{
	        public void actionPerformed(ActionEvent e)
	        {
	        	yea.setVisible(true);
	        }
		}
		
		class Ok_Billuri implements ActionListener
		{

			public void actionPerformed(ActionEvent arg0) {
				
				int nr_masa = Integer.parseInt(tf.getText());
				yea.setVisible(false);
	        	try {
					Dinel.billz(nr_masa);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
	
		Billuri a3= new Billuri();
		Ok_Billuri a4 = new Ok_Billuri();
		bill.addActionListener(a3);
		Ok_Bill.addActionListener(a4);
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Admin a = new Admin();

		
	}
}